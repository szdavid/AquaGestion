# Creation de la table utilisateurs
    CREATE TABLE Users (
      user_id INT NOT NULL AUTO_INCREMENT,
      email VARCHAR(80) NOT NULL,
      display_name VARCHAR(50) NOT NULL,
      password CHAR(41) NOT NULL,
      PRIMARY KEY (user_id),
      UNIQUE INDEX (email)
    ) ENGINE=INNODB;


    CREATE TABLE UsersExtra (
      user_id INT NOT NULL,
      first_name VARCHAR(25),
      last_name VARCHAR(25),
      address VARCHAR(80),
      city VARCHAR(30),
      province CHAR(2),
      postcode CHAR(7),
      interests TEXT NULL,
      bio TEXT NULL,
      signature TEXT NULL,
      skills TEXT NULL,
      PRIMARY KEY (user_id),
      FULLTEXT KEY (interests, skills),
      CONSTRAINT fk_users FOREIGN KEY (user_id) REFERENCES users (user_id)
    ) ENGINE=MyISAM;

    CREATE TABLE hardware_type (
      type_h_id int not null auto_increment,
      type_name varchar(50),
      PRIMARY KEY (type_h_id)
    );

    INSERT into hardware_type (type_name) VALUES ('Autre');
    INSERT into hardware_type (type_name) VALUES ('Filtre');
    INSERT into hardware_type (type_name) VALUES ('Distributeur de nourriture');
    INSERT into hardware_type (type_name) VALUES ('Chauffage');
    INSERT into hardware_type (type_name) VALUES ('Jardinage');
    INSERT into hardware_type (type_name) VALUES ('Eclairage');


    CREATE TABLE decoration_type (
      type_deco_id int not null auto_increment,
      deco_name varchar(50),
      PRIMARY KEY (type_deco_id)
    );

    INSERT into decoration_type (deco_name) VALUES ('Autre');
    INSERT into decoration_type (deco_name) VALUES ('Souche');
    INSERT into decoration_type (deco_name) VALUES ('Pierre');


    CREATE TABLE sex (
      sex_id int not null auto_increment,
      sex_name varchar(10),
      PRIMARY KEY (sex_id)
    );

    INSERT into sex (sex_name) VALUES ('Indéterminé');
    INSERT into sex (sex_name) VALUES ('Mâle');
    INSERT into sex (sex_name) VALUES ('Femelle');

    CREATE TABLE positions (
      position_id int not null auto_increment,
      position_name varchar(10),
      PRIMARY KEY (position_id)
    );

    INSERT into positions (position_name) VALUES ('Avant-droit');
    INSERT into positions (position_name) VALUES ('Avant-centre');
    INSERT into positions (position_name) VALUES ('Avant-gauche');
    INSERT into positions (position_name) VALUES ('Milieu-droit');
    INSERT into positions (position_name) VALUES ('Milieu-centre');
    INSERT into positions (position_name) VALUES ('Milieu-gauche');
    INSERT into positions (position_name) VALUES ('Arrière-droit');
    INSERT into positions (position_name) VALUES ('Arrière-centre');
    INSERT into positions (position_name) VALUES ('Arrière-gauche');

    CREATE TABLE death_reasons (
      death_reason_id int not null auto_increment,
      death_reason varchar(10),
      PRIMARY KEY (death_reason_id)
    );

    INSERT into death_reason (death_reason) VALUES ('Maladie');
    INSERT into death_reasons (death_reason) VALUES ('Disparition');
    INSERT into death_reasons (death_reason) VALUES ('Inconnue');

    CREATE TABLE product_type (
      type_pr_id int not null auto_increment,
      type_name varchar(50),
      PRIMARY KEY (type_h_id)
    );

    INSERT into product_type (type_name) VALUES ('Autre');
    INSERT into product_type (type_name) VALUES ('Jardinage');
    INSERT into product_type (type_name) VALUES ('Nourriture');
    INSERT into product_type (type_name) VALUES ('Médicaments');
    INSERT into product_type (type_name) VALUES ('Tests d\'eau');
    INSERT into product_type (type_name) VALUES ('Traitement de l\'eau');

CREATE TABLE zones (
      zone_id int not null auto_increment,
      zone_name varchar(50),
      PRIMARY KEY (zone_id)
    );

    INSERT into zones (zone_name) VALUES ('Fond');
    INSERT into zones (zone_name) VALUES ('Surface');



    CREATE TABLE scientific_fishes (
      scientific_id int not null auto_increment,
      scientific_name varchar(100),
      common_name varchar (100)
      min_temp int,
      max_temp int,
      min_ph decimal (2,1),
      max_ph decimal(2,1)
      min_durete int,
      max_durete int,
      zone_id int,
      lifetime int,
      size int,
      behavior text,
      reproduction text,
      comment text,
      PRIMARY KEY (scientific_id),
      CONSTRAINT fk_fi_z FOREIGN KEY (zone_id) REFERENCES zones (zone_id)
    );

    CREATE TABLE scientific_plants (
      scientific_id int not null auto_increment,
      scientific_name varchar(100),
      common_name varchar (100)
      min_temp int,
      max_temp int,
      min_ph decimal (2,1),
      max_ph decimal(2,1)
      min_durete int,
      max_durete int,
      size int,
      comment text,
      PRIMARY KEY (scientific_id)
    );



    CREATE table brands (
      brand_id int not null auto_increment,
      brand_name varchar(50) not null,
      address varchar(100),
      address_2 varchar(100),
      postal_code int,
      city varchar(100),
      country varchar (100),
      phone varchar (50),
      fax varchar(50),
      email varchar (100),
      www varchar(200),
      rating int,
      comment text,
      PRIMARY KEY (brand_id)
    );


    CREATE table shops (
      shop_id int not null auto_increment,
      shop_name varchar(50) not null,
      address varchar(100),
      address_2 varchar(100),
      postal_code int,
      city varchar(100),
      country varchar (100),
      phone varchar (50),
      fax varchar(50),
      email varchar (100),
      www varchar(200),
      opening text, /* Horaires d'ouverture*/
      vpc boolean null,
      rating int,
      comment text,
      PRIMARY KEY (shop_id)
    );

    CREATE TABLE aquarium (
      aquarium_id int not null auto_increment,
      aquarium_name varchar(50) not NULL,
      length int,
      width int,
      heighth int,
      calculated_volume int,
      useful_volume int,
      water_volume int,
      creation_date TIMESTAMP DEFAULT NOW(),
      purchase_date date,
      filling_date date,
      location varchar(200),
      description text,
      is_active boolean,
      brand_id int,
      shop_id int,
      rating int,
      price decimal (5,2),
      PRIMARY KEY (aquarium_id),
      CONSTRAINT fk_aq_shop FOREIGN KEY (shop_id) REFERENCES shops (shop_id),
      CONSTRAINT fk_aq_brands FOREIGN KEY (brand_id) REFERENCES brands (brand_id)
    );




    CREATE TABLE hardware (
      hardware_id int not null auto_increment,
      aquarium_id int,
      brand_id int,
      shop_id int,
      price decimal (5,2),
      purchase_date date,
      first_use_date date,
      end_date date,
      consommation_electrique decimal (5,2),
      flow decimal (5,2),
      type_h_id int not null,
      comment text,
      rating int,
      PRIMARY KEY (hardware_id),
      CONSTRAINT fk_hw_aq FOREIGN KEY (aquarium_id) REFERENCES aquarium (aquarium_id),
      CONSTRAINT fk_hw_shop FOREIGN KEY (shop_id) REFERENCES shops (shop_id),
      CONSTRAINT fk_hw_brands FOREIGN KEY (brand_id) REFERENCES brands (brand_id),
      CONSTRAINT fk_hw_type FOREIGN KEY (type_h_id) REFERENCES hardware_type (type_h_id)
    );

    CREATE TABLE products (
      product_id int not null auto_increment,
      aquarium_id int,
      brand_id int,
      shop_id int,
      price decimal (5,2),
      purchase_date date,
      first_use_date date,
      end_date date,
      limit_date date,
      type_pr_id int not null,
      comment text,
      rating int,
      PRIMARY KEY (hardware_id),
      CONSTRAINT fk_pr_aq FOREIGN KEY (aquarium_id) REFERENCES aquarium (aquarium_id),
      CONSTRAINT fk_pr_shop FOREIGN KEY (shop_id) REFERENCES shops (shop_id),
      CONSTRAINT fk_pr_brands FOREIGN KEY (brand_id) REFERENCES brands (brand_id),
      CONSTRAINT fk_pr_type FOREIGN KEY (type_pr_id) REFERENCES product_type (type_h_id)
    );



    CREATE TABLE fishes (
      fish_id int not null auto_increment,
      aquarium_id int,
      name VARCHAR(50),
      scientific_id int,
      shop_id int,
      price decimal (5,2),
      sex_id int not null DEFAULT 1,
      size decimal (5,2),
      purchase_date date null,
      birth_date date null,
      death_date date null,
      death_reason_id int,
      comment text,
      PRIMARY KEY (fish_id),
      CONSTRAINT fk_po_aq FOREIGN KEY (aquarium_id) REFERENCES aquarium (aquarium_id),
      CONSTRAINT fk_po_shop FOREIGN KEY (shop_id) REFERENCES shops (shop_id),
      CONSTRAINT fk_po_sx FOREIGN KEY (sex_id) REFERENCES sex (sex_id),
      CONSTRAINT fk_po_dt FOREIGN KEY (death_reason_id) REFERENCES death_reasons (death_reason_id),
      CONSTRAINT fk_po_sc FOREIGN KEY (scientific_id) REFERENCES scientific_fishes (scientific_id)

    );

CREATE TABLE plants (
      plant_id int not null auto_increment,
      aquarium_id int,
      name VARCHAR(50),
      scientific_id int,
      shop_id int,
      price decimal (5,2),
      size decimal (5,2),
      purchase_date date,
      end_date date null,
      position int,
      comment text,
      PRIMARY KEY (fish_id),
      CONSTRAINT fk_pl_aq FOREIGN KEY (aquarium_id) REFERENCES aquarium (aquarium_id),
      CONSTRAINT fk_pl_shop FOREIGN KEY (shop_id) REFERENCES shops (shop_id),
      CONSTRAINT fk_pl_sx FOREIGN KEY (sex_id) REFERENCES sex (sex_id),
      CONSTRAINT fk_pl_dt FOREIGN KEY (death_reason_id) REFERENCES death_reasons (death_reason_id),
      CONSTRAINT fk_pl_sc FOREIGN KEY (scientific_id) REFERENCES scientific_plants (scientific_id),
      CONSTRAINT fk_pl_position FOREIGN KEY (position_id) REFERENCES positions (position_id)
    );

    CREATE TABLE seafood (
      seafood_id int not null auto_increment,
      aquarium_id int,
      name VARCHAR(50),
      scientific_id int,
      shop_id int,
      price decimal (5,2),
      sex_id int not null DEFAULT 1,
      size decimal (5,2),
      purchase_date date,
      birth_date date null,
      death_date date null,
      death_reason_id int,
      comment text,
      PRIMARY KEY (seafood_id),
      CONSTRAINT fk_sf_aq FOREIGN KEY (aquarium_id) REFERENCES aquarium (aquarium_id),
      CONSTRAINT fk_sf_shop FOREIGN KEY (shop_id) REFERENCES shops (shop_id),
      CONSTRAINT fk_sf_sx FOREIGN KEY (sex_id) REFERENCES sex (sex_id),
      CONSTRAINT fk_sf_dt FOREIGN KEY (death_reason_id) REFERENCES death_reasons (death_reason_id),
      CONSTRAINT fk_sf_sc FOREIGN KEY (scientific_id) REFERENCES scientific_fishes (scientific_id)

    );


CREATE TABLE decoration (
      decoration_id int not null auto_increment,
      aquarium_id int,
      brand_id int,
      shop_id int,
      price decimal (5,2),
      purchase_date date,
      end_date date null,
      type_deco_id int not null default 1,
      comment text,
      PRIMARY KEY (decoration_id),
      CONSTRAINT fk_deco_aq FOREIGN KEY (aquarium_id) REFERENCES aquarium (aquarium_id),
      CONSTRAINT fk_deco_shop FOREIGN KEY (shop_id) REFERENCES shops (shop_id),
      CONSTRAINT fk_deco_brands FOREIGN KEY (brand_id) REFERENCES brands (brand_id),
      CONSTRAINT fk_deco_type FOREIGN KEY (type_deco_id) REFERENCES decoration_type (type_deco_id)
    );


CREATE TABLE analysis (
  analysis_id int not null auto_increment,
  aquarium_id int,
  analysis_date TIMESTAMP DEFAULT NOW(),
  temp decimal(2,2),
  gh decimal(2,2),
  kh decimal(2,2),
  o2 decimal(2,2),
  co2 decimal(2,2),
  fe decimal(2,2),
  cu decimal(2,2),
  densite int,
  ca decimal(2,2),
  i2 decimal(2,2),
  ph decimal(2,2),
  NH4 decimal(2,2),
  NO2 decimal(2,2),
  NO3 decimal(2,2),
  PO4 decimal(2,2),
  conductivite decimal(2,2),
  sio2 decimal(2,2),
  salinite int,
  mg int,
  sr decimal(2,2),
  b decimal(2,2),
  comment text,
  PRIMARY KEY (analysis_id),
  CONSTRAINT fk_an_aq FOREIGN KEY (aquarium_id) REFERENCES aquarium (aquarium_id)
);


CREATE TABLE entretien (
  entretien_id int not null auto_increment,
  aquarium_id int,
  entretien_date TIMESTAMP DEFAULT NOW(),
  comment text,
  PRIMARY KEY (entretien_id),
  CONSTRAINT fk_en_aq FOREIGN KEY (aquarium_id) REFERENCES aquarium (aquarium_id)
);

# Un entretien peut impliquer plusieurs produits et pas nécessairement un seul
CREATE TABLE entretien_produit (
  ep_id int not null auto_increment,
  entretien_id int,
  product_id int,
  comment text,
  PRIMARY KEY (ep_id),
  CONSTRAINT fk_ep_en FOREIGN KEY (entretien_id) REFERENCES entretien (entretien_id),
  CONSTRAINT fk_ep_pr FOREIGN KEY (product_id) REFERENCES products(product_id)
);


# Un entretien peut impliquer plusieurs appareils et pas nécessairement un seul
CREATE TABLE entretien_materiel (
  em_id int not null auto_increment,
  entretien_id int,
  hardware_id int,
  comment text,
  PRIMARY KEY (em_id),
  CONSTRAINT fk_em_en FOREIGN KEY (entretien_id) REFERENCES entretien (entretien_id),
  CONSTRAINT fk_em_ha FOREIGN KEY (hardware_id) REFERENCES hardware(hardware_id)
);
