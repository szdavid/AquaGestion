# AquaGestion

## Fonctionnalités à développer

### Base de données

- [x] Gestion des aquariums
- [x] Gestion des poissons
- [x] Gestion des alevins
- [x] Gestion des plantes
- [x] Gestion des crustacés
- [x] Gestion des décors
- [ ] Gestion des escargots
- [ ] Gestion des alevins (?)
- [x] Enregistrement des paramètres de l'eau

### Interface graphique

- [ ] Création d'un aquarium
- [ ] Mise à jour d'un aquarium
- [ ] Suppression ou désactivation d'un aquarium
- [ ] Insertion de résultats de Tests
- [ ] Suppression de Tests
- [ ] Déplacement de tests dans un autre aquarium
- [ ] Insertion de poissons
- [ ] Mise à jour de poissons
- [ ] Insertion de plantes
- [ ] Mise à jour de plantes
- [ ] Insertion de crustacés
- [ ] Mise à jour de crustacés
- [ ] Base de poissons et de crustacés
- [ ] Base de plantes
- [ ] Gestion des alevins ?
- [ ] Modification de son profil

### Administration
- [ ] Ajout d'un utilisateur
- [ ] Modification d'un utilisateur
